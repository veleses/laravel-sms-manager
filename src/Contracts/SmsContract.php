<?php

namespace Yole\LaravelSms\Contracts;

interface SmsContract
{
    public function send($phone = null, string $message = '', array $config = []);
}