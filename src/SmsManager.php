<?php

namespace Yole\LaravelSms;

use Closure;
use InvalidArgumentException;

class SmsManager
{

    protected $app;
    protected $drivers = [];
    protected $customCreators = [];

    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @param null $driver
     * @return mixed
     */

    public function connection($driver = null)
    {
        return $this->driver($driver);
    }


    /**
     * @param null $name
     * @return mixed
     */

    public function driver($name = null)
    {
        $name = $name ?: $this->getDefaultDriver();

        return $this->drivers[$name] = $this->get($name);
    }


    /**
     * @param $name
     * @return mixed
     */

    protected function get($name)
    {
        return $this->drivers[$name] ?? $this->resolve($name);
    }


    /**
     * @param string $name
     * @param array $config
     * @return mixed
     */

    protected function resolve(string $name, array $config = [])
    {
        $config = $config ?? $this->getConfig($name);

        if (is_null($config)) {
            throw new InvalidArgumentException("SMS driver [{$name}] is not defined.");
        }

        if (isset($this->customCreators[$config['driver']])) {
            return $this->callCustomCreator($config);
        }

        $driverName = ucfirst($config['driver']).'Driver';

        return $this->create($driverName, $config);
    }


    /**
     * @param array $config
     * @return mixed
     */

    protected function callCustomCreator(array $config)
    {
        return $this->customCreators[$config['driver']]($this->app, $config);
    }

    /**
     * @param $name
     * @return mixed
     */

    protected function getConfig($name)
    {
        return $this->app['config']["sms.connections.{$name}"];
    }


    /**
     * @return mixed
     */

    public function getDefaultDriver()
    {
        return $this->app['config']['sms.default'];
    }


    /**
     * @param $name
     */

    public function setDefaultDriver($name) : void
    {
        $this->app['config']['sms.default'] = $name;
    }


    /**
     * @param $driver
     * @param Closure $callback
     * @return $this
     */

    public function extend($driver, Closure $callback) : self
    {
        $this->customCreators[$driver] = $callback;

        return $this;
    }

    /**
     * @param $name
     * @param $config
     * @return mixed
     */

    private function create($name, $config)
    {
        if (class_exists($name)) {
            return new $name($config);
        }

        throw new InvalidArgumentException("Driver [{$config['driver']}] is not supported.");
    }

    /**
     * @param $method
     * @param $parameters
     * @return mixed
     */

    public function __call($method, $parameters)
    {
        return $this->driver()->$method(...$parameters);
    }
}
