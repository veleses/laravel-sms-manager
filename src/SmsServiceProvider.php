<?php

namespace Yole\LaravelSms;

use Illuminate\Support\ServiceProvider;
use Yole\LaravelSms\Drivers\SmsContract;

class SmsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/sms.php' => config_path('sms.php'),
        ], 'sms');
    }


    public function register() {
        $this->mergeConfigFrom( __DIR__.'/../config/sms.php', 'sms');

        $this->app->singleton(SmsManager::class, function ($app) {
            return new SmsManager($app);
        });

        $this->app->singleton('sms', function ($app) {
            return $app->make(SmsManager::class)->connection();
        });
    }

    public function provides() {
        return ['sms'];
    }
}
