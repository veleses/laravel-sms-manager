<?php

namespace Yole\LaravelSms\Drivers;

use GuzzleHttp\Client;

class MtsSms extends SmsDriver
{

    private $client, $params;

    public function __construct($password)
    {
        $this->client = new Client([
            'base_uri' => 'https://api.mcommunicator.ru/m2m/m2m_api.asmx/'
        ]);

        $this->params = ['password' => $password];

    }

    public function send($phone = null, string $message = '', array $config= [])
    {
        return $this->client->request('GET', 'SendMessage', [
            'query' => array_merge([
                'msid' => $phone,
                'message' => $message,
                'naming' => $config['naming']?? '' ,
            ], $this->params)
        ]);
    }

    public function getMessageStatus($message_id = null)
    {
        if (!$message_id) {
            return false;
        }


        if (\is_array($message_id)) {

            return $this->client->request('GET', 'GetMessagesStatus', [
                'query' => array_merge([
                    'messageIDs' => $message_id
                ], $this->params)
            ]);

        }

        return $this->client->request('GET', 'GetMessagesStatus', [
            'query' => array_merge([
                'messageID' => $message_id
            ], $this->params)
        ]);

    }

    public function sendMultiple(array $phones, string $message = '')
    {
        if (empty($phones) || empty($message)) {
            return false;
        }

        $phones = $this->phoneParseArray($phones);

        return $this->client->request('GET', 'SendMessages', [
            'query' => array_merge([
                'msids' => $phones,
                'message' => $message
            ], $this->params)
        ]);
    }

    public function addUser(
        string $username = '',
        string $phone = '',
        string $email = '',
        $user_group = null,
        $base_user = 'BaseUser'
    ) {
        if (empty($phone) || empty($username)) {
            return false;
        }

        if (!in_array($base_user, ['Administrator', 'Operator', 'BaseUser'])) {
            return false;
        }


        if (!$user_group) {
            $user_group = $this->default_user_group;
        }

        return $this->client->request('GET', 'AddUser', [
            'query' => array_merge([
                'userName' => $username,
                'userMSID' => $this->phoneParse($phone),
                'userEmail' => $email,
                'accessLevel' => $base_user,
                'userGroupId' => $user_group,
                'webAccessEnabled' => false,
            ], $this->params)
        ]);
    }

    private function phoneParse($phone = '')
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        $replace = preg_replace('/^([8]{1})([0-9]{10}$)/', '7$2', $phone);

        if (!empty($replace)) {
            $phone = $replace;
        }

        return $phone;
    }

    private function phoneParseArray(array $phones)
    {
        $phones = array_filter($phones, function($phone) {
            return !empty($phone);
        });

        return array_map(function ($phone) {
            return $this->phoneParse($phone);
        }, $phones);
    }
}
