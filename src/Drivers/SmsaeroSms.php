<?php

namespace Yole\LaravelSms\Drivers;

use GuzzleHttp\Client;

class SmsaeroSms extends SmsDriver
{
    private $client, $sign;

    public function __construct($login, $password, $sign)
    {
        $this->client = new Client([
            'base_uri' => "https://{$login}:{$password}@gate.smsaero.ru/v2/"
        ]);

        $this->sign = $sign;
    }

    public function login()
    {
        return $this->client->request('POST', 'SendMessage');
    }

    public function send($phone = null, string $message = '', array $config = [])
    {

        if (isset($config['sign']) && !empty($config['sign'])) {
            $sign = $config['sign'];
        } else {
            $sign = $this->sign;
        }

        if (isset($config['channel']) && $this->correctChannel($config['channel'])) {
            $channel = $config['channel'];
        } else {
            $channel = 'INFO';
        }

        $options = [
            'sign' => $sign,
            'text' => $message,
            'channel' => $channel
        ];

        if (\is_array($phone)) {
            $options['numbers'] = $phone;
        } else {
            $options['number'] = $phone;
        }

        return json_decode($this->client->request('POST', 'send', [
            'json' => $options
        ]), true);

    }

    private function correctChannel($channel = '')
    {
        return \in_array($channel, ['INFO', 'DIGITAL', 'INTERNATIONAL', 'DIRECT', 'SERVICE']) ? true : false;
    }
}